let checklistID;
const cardId = '5b90dab1377a078c7de3e663';
const key = '67c88d2d75377f0a99a6f45274738749';
const token = '0f0f033068d9fadc70e7893ac2aec354d6160ab047276114ca9d9784a6101da8';
const url = 'https://api.trello.com/1/members/me/boards';
fetch(`${url}?key=${key}&token=${token}`)
    .then(response => response.json())
    .then((boards) => {
        const boardID = boards[0].id;
        fetch(`https://api.trello.com/1/boards/${boardID}/cards?key=${key}&token=${token}`)
            .then(response => response.json())
            .then((cards) => {
                const cardID = cards[0].id;
                $.get(`https://api.trello.com/1/cards/${cardID}/checklists?key=${key}&token=${token}`, (checklists) => {
                    checklists.forEach((checklist) => {
                        checklistID = checklist.id;
                        const items = checklist.checkItems;
                        items.forEach((item) => {
                            const checked = item.state === 'complete' ? 'checked' : '';
                            const taskid = item.id;
                            const task = item.name;
                            const checkbox = `<li class="list-group-item"><input type='checkbox' class='form-check-input' id=${taskid} ${checked}> <label>${task} </label></li>`;
                            $('ul').append(checkbox);
                        });
                    });
                });
            })
            .catch((err) => {
                console.log(err);
            });
    });



$(':text').blur(() => {
    const data = $('#dataTobeAdded').val();
    if (data.length === 0) {
        $('.btn-success').css("opacity", 0);
    }
});
$(':text').focus(() => {
    $('.btn-success').css("opacity", 1);

});

$('#data').submit((element) => {
    element.preventDefault();
    const data = $('#dataTobeAdded').val();
    // console.log(data);
    fetch(`https://api.trello.com/1/checklists/${checklistID}/checkItems?name=${data}&key=${key}&token=${token}`, {
        method: 'post',
    });

    $('ul').append(`<li class="list-group-item"><input type="checkbox" class="form-check-input" ><label>${data}</label></li>`);
    document.getElementById('dataTobeAdded').value = '';

});

$(document).on("change", "input[type=checkbox]", () => {
    if (this.checked) {
        const taskID = $(this).attr('id');
        fetch(`https://api.trello.com/1/cards/${cardId}/checklist/${checklistID}/checkItem/${taskID}/state?value=complete&key=${key}&token=${token}`, {
                method: 'put',
            })
            .then(response => response.json())
            .catch((err) => {
                console.log(err);
            });
    } else {
        const taskID = ($(this).attr('id'));
        fetch(`https://api.trello.com/1/cards/${cardId}/checklist/${checklistID}/checkItem/${taskID}/state?value=incomplete&key=${key}&token=${token}`, {
                method: 'put',
            })
            .then(response => response.json())
            .catch((err) => {
                console.log(err);
            });
    }
});